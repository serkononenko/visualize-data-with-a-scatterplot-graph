document.addEventListener('DOMContentLoaded', async function() {
    const response = await fetch(' https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/cyclist-data.json');
    const dataset =  await response.json();
    console.log(dataset)

    const width = 900;
    const height = 400;
    const padding = 60;

    const svg = d3.select('.container').append('svg')
        .attr('class', 'svg')
        .attr('width', width)
        .attr('height', height);

    const minYear = d3.min(dataset, d => d.Year);
    const maxYear = d3.max(dataset, d => d.Year);

    const xScale = d3.scaleTime()
        .domain([new Date(minYear - 1, 0, 0), new Date(maxYear + 1, 0, 0)])
        .range([padding, width - padding]);

    const minSecond = d3.min(dataset, d => d.Seconds);
    const maxSecond = d3.max(dataset, d => d.Seconds);

    const yScale = d3.scaleTime()
        .domain([new Date(maxSecond * 1000), new Date(minSecond * 1000)])
        .range([height - padding, 0]);

    const xAxis = d3.axisBottom(xScale);
    const yAxis = d3.axisLeft(yScale)
//        .tickArguments([d3.timeSecond.every(15)])
        .tickFormat(d3.timeFormat('%M:%S'));

    svg.selectAll('circle')
        .data(dataset)
        .enter()
        .append('circle')
        .attr('class', 'dot')
        .attr('cx', (d, i) => xScale(new Date(d.Year, 0, 0)))
//        .attr('cy', d => yScale(new Date(0, 0, 0, 0, 0, d.Seconds)))
        .attr('cy', d => yScale(new Date(d.Seconds * 1000)))
        .attr('r', 6)
        .attr('fill', d => d.Doping ? 'cornflowerblue' : 'orange')
        .attr('data-xvalue', d => d.Year)
        .attr('data-yvalue', d => new Date(d.Seconds * 1000))

    svg.append("g")
        .attr("id", "x-axis")
        .attr("transform", `translate(${0}, ${height - padding})`)
        .call(xAxis);

    svg.append("g")
        .attr("id", "y-axis")
        .attr("transform", `translate(${padding}, 0)`)
        .call(yAxis);

    const legend1 = svg.append("g")
        .attr("id", "legend")
        .attr("transform", `translate(${width - padding}, ${height / 2})`);

    legend1.append('rect')
        .attr("width", "12")
        .attr("height", "12")
        .attr("fill", "orange");

    legend1.append('text')
        .text('No doping allegations')
        .attr("transform", `translate(${- 210}, ${10})`);

    const legend2 = svg.append("g")
        .attr("id", "legend")
        .attr("transform", `translate(${width - padding}, ${height / 3})`);

    legend2.append('rect')
        .attr("width", "12")
        .attr("height", "12")
        .attr("fill", "cornflowerblue");

    legend2.append('text')
        .text('Riders with doping allegations')
        .attr("transform", `translate(${- 210}, ${10})`);

    const dots = document.querySelectorAll('.dot');
    const tooltip = document.querySelector('#tooltip');
    dots.forEach(dot => {
        dot.onmouseover = () => {
            const { xvalue, yvalue } = dot.dataset;
            tooltip.setAttribute('data-year', xvalue);
            tooltip.innerHTML = `<p>Year: ${xvalue}</p><p>Time: ${yvalue}</p>`;
            tooltip.classList.add('tooltip-show');
//            tooltip.style.left = `${dot.getAttribute('cx')}px`;
//            tooltip.style.top = `${dot.getAttribute('cy') + padding}px`;
        };
        dot.onmouseout = () => {
            tooltip.classList.remove('tooltip-show');
        };
    })
})